var currPage = 0;
var pageCout = $('.page').length;
var pages = $('.page');

if(localStorage.lastOpenPage) {
	if (localStorage.lastOpenPage > pageCout - 1) {
		localStorage.lastOpenPage = pageCout - 1
	}
	currPage = localStorage.lastOpenPage;
}

//side navigation bar is hiden defaultly.
//$('div#outline').hide();
//Hide all pages inorder to make the current page fadein-effect.
pages.hide();
pages.eq(currPage).fadeIn();

//鼠标点击事件
//$('.page').click(function(e) {
//	pages.eq(currPage).fadeOut(function() {
//		if(currPage < pageCout - 1)
//			currPage++;
//		pages.eq(currPage).fadeIn();
//	});
////	$('body').fadeOut();
////	if (e.pageX > screen.availWidth / 2) {
////		if (currPage < pageCout - 1)
////			currPage++;
////	} else{
////		if (currPage > 0)
////		 	currPage--;
////	}
////	if (currPage < pageCout - 1)
////			currPage++;
//////	$('body').fadeIn();
////	pages.eq(currPage).fadeIn();
//});

//鼠标滚轮事件
//document.body.onmousewheel = function(event) {
//	//	if((currPage == pageCout - 1) || (event.deltaY == 0))
//	//		return;
//	pages.eq(currPage).fadeOut(function() {
//		if(event.deltaY > 0) { //向下滚动
//			if(currPage < pageCout - 1)
//				currPage++;
//		} else if(event.deltaY < 0) { //向上滚动
//			if(currPage > 0)
//				currPage--;
//		}
//		localStorage.lastOpenPage = currPage;
//		pages.eq(currPage).fadeIn();
//	});
//};

//switch previous page or next one?
function switchPage(pre_next) {
	pages.eq(currPage).fadeOut(function () {
	switch(pre_next) {
		case "next":
			if(currPage < pageCout - 1)
				currPage++;
			break;
		case "previous":
			if(currPage > 0)
				currPage--;
			break;
		default:
			break;
	}
	localStorage.lastOpenPage = currPage;
	
		pages.eq(currPage).fadeIn();
	});
}
//鼠标移动到左侧时，显示导航栏
//document.body.onmousemove = function(event) {
//	if(event.pageX - 100 < 0) { /////event.pageX + 100 > screen.availWidth///
//		$('div#outline').fadeIn();
//	} else {
//		$('div#outline').fadeOut();
//	}
//};

function sideNavBarShowToggle(){
	if ($("div#outline").css("display") == "none") {
		$('div#outline').fadeIn();
	} else{
		$('div#outline').fadeOut();
	}
}

//key settings
hotkeys("`,esc,left,right", function(event, handler) {
	switch(handler.key) {
		case "`":
			sideNavBarShowToggle();
			break;
		case "esc":
			
			break;
		case "left":
			switchPage("previous");
			break;
		case "right":
			switchPage("next");
			break;
		default:
			break;
	}
});